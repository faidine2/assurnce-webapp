﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAssurNce.Models
{
    public class AssurNceDBContext:DbContext
    {
        public AssurNceDBContext(DbContextOptions<AssurNceDBContext> options): base(options)
        {

        }

        public DbSet<Insurance> insurances { get; set; }
    }
}
