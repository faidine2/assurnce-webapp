﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAssurNce.Models;

namespace WebAssurNce.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InsuranceController : ControllerBase
    {
        private readonly AssurNceDBContext _context;

        public InsuranceController(AssurNceDBContext context)
        {
            _context = context;
        }

        // GET: api/Insurance
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Insurance>>> Getinsurances()
        {
            return await _context.insurances.ToListAsync();
        }

        // GET: api/Insurance/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Insurance>> GetInsurance(int id)
        {
            var insurance = await _context.insurances.FindAsync(id);

            if (insurance == null)
            {
                return NotFound();
            }

            return insurance;
        }

        // PUT: api/Insurance/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutInsurance(int id, Insurance insurance)
        {
           
            insurance.id = id;

            _context.Entry(insurance).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InsuranceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Insurance
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Insurance>> PostInsurance(Insurance insurance)
        {
            _context.insurances.Add(insurance);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetInsurance", new { id = insurance.id }, insurance);
        }

        // DELETE: api/Insurance/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Insurance>> DeleteInsurance(int id)
        {
            var insurance = await _context.insurances.FindAsync(id);
            if (insurance == null)
            {
                return NotFound();
            }

            _context.insurances.Remove(insurance);
            await _context.SaveChangesAsync();

            return insurance;
        }

        private bool InsuranceExists(int id)
        {
            return _context.insurances.Any(e => e.id == id);
        }
    }
}
