import React, { useEffect } from "react";
import { Grid, TextField, withStyles, FormControl, Select, Button, InputLabel, MenuItem, FormHelperText } from "@material-ui/core";
import useForm from "./useForm";
import { connect } from "react-redux";
import * as actions from "../actions/insurance";
import { useToasts } from "react-toast-notifications";

const styles = theme => ({
    root: {
    '& .MuiTextField-root': {
        margin: theme.spacing(1),
        minWidth: 230,
        }
    },
    formControl:{
        margin: theme.spacing(1),
        minWidth: 230,
    },
    smMargin:{
        margin: theme.spacing(1),
    }
})

const initialFieldValues = {
    fullName : '',
    mobile : '',
    email : '',
    age: '',
    insuranceType: '',
    address: ''
}

const InsuranceForm = ({classes, ...props}) => {

    const { addToast } = useToasts();

    const validate = (fieldValues = values) => {
        let temp = { ...errors }
        if ('fullName' in fieldValues)
        temp.fullName = fieldValues.fullName ? "" : "This field is required."
        if ('mobile' in fieldValues)
        temp.mobile = fieldValues.mobile ? "" : "This field is required."
        if ('insuranceType' in fieldValues)
        temp.insuranceType = fieldValues.insuranceType ? "" : "This field is required."
        if ('email' in fieldValues)
        temp.email = (/^$|.+@.+..+/).test(fieldValues.email) ? "" : "E-mail is not valid."
        setErrors({
            ...temp
        })
        if (fieldValues == values)
        return Object.values(temp).every(x => x == "")
     }

    const {
        values,
        setValues,
        errors,
        setErrors,
        handleInputChange,
        resetForm
    } = useForm(initialFieldValues, validate, props.setCurrentId)

    const inputLabel = React.useRef(null);
    const [labelWidth, setLabelWidth] = React.useState(0);
    React.useEffect(() =>{
        setLabelWidth(inputLabel.current.offsetWidth);
    }, []);

    const handleSubmit = e => {
        e.preventDefault()
        if(validate())
        {
            const onSuccess = () => { 
                resetForm()
                addToast("Submitted successfully, you will be contacted soon for more information.", {appearance: 'success'})
            } 
            if(props.currentId==0)
            props.createInsurance(values, onSuccess)
            else
            props.updateInsurance(props.currentId, values, onSuccess)
        }
    }

    useEffect(()=>{
        if (props.currentId != 0) {
        setValues({
            ...props.insuranceList.find(x => x.id == props.currentId)
        })
        setErrors({})
        }
    }, [props.currentId])

    return (
        <form autoComplete="off" noValidate className={classes.root} onSubmit={handleSubmit}>
            <Grid container>
                <Grid item xs={6}>
                    <TextField 
                    name="fullName"
                    variant="outlined"
                    label="Full Name"
                    value={values.fullName}
                    onChange={handleInputChange}
                    {...(errors.fullName &&  {error: true, helperText: errors.fullName })}
                    />
                    <TextField 
                    name="email"
                    variant="outlined"
                    label="E-mail"
                    value={values.email}
                    onChange={handleInputChange}
                    {...(errors.email &&  {error: true, helperText: errors.email })}
                    />
                    <FormControl variant="outlined" 
                    className={classes.formControl}
                    {...(errors.insuranceType &&  {error: true })}>
                        <InputLabel ref={inputLabel}>Insurance Type</InputLabel>
                        <Select 
                            name="insuranceType"
                            value={values.insuranceType}
                            onChange={handleInputChange}
                            labelWidth={labelWidth}
                            >
                                <MenuItem value="">Select Insurance Type</MenuItem>
                                <MenuItem value="Vehicule">Vehicule</MenuItem>
                                <MenuItem value="Home">Home</MenuItem>
                                <MenuItem value="Life">Life</MenuItem>
                                <MenuItem value="Health">Health</MenuItem>
                                <MenuItem value="Travel">Travel</MenuItem>
                        </Select>
                        {errors.insuranceType && <FormHelperText>{errors.insuranceType}</FormHelperText>}
                    </FormControl>
                </Grid>
                <Grid item xs={6}>
                    <TextField name="mobile"
                    variant="outlined"
                    label="Mobile"
                    value={values.mobile}
                    onChange={handleInputChange}
                    {...(errors.mobile &&  {error: true, helperText: errors.mobile })}
                    />
                    <TextField name="age"
                    variant="outlined"
                    label="Age"
                    value={values.age}
                    onChange={handleInputChange}
                    />
                    <TextField name="address"
                    variant="outlined"
                    label="Address"
                    value={values.address}
                    onChange={handleInputChange}
                    />
                    <div>
                        <Button 
                        variant="contained"
                        color="primary"
                        type="submit"
                        className={classes.smMargin}>
                            Submit
                        </Button>
                        <Button 
                            variant="contained"
                            className={classes.smMargin}
                            onClick={resetForm}
                            >
                            Reset
                        </Button>
                    </div>
                </Grid>
            </Grid>
        </form>
    );
}

const mapStateToProps = state => ({
    insuranceList: state.insurance.list
})

const mapActionToProps = {
    createInsurance: actions.create,
    updateInsurance: actions.update
}

export default connect(mapStateToProps, mapActionToProps)(withStyles(styles)(InsuranceForm));