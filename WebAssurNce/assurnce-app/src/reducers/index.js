import { combineReducers } from "redux";
import { insurance } from "./insurance";

export const reducers = combineReducers({insurance})