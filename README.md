# AssurNce Web Application

AssurNce is a insurance company created for this project.

**Version 1.0.0**

This README is for the Web application AssurNce which is a sample of a first step to take out insurance online.

---

* AssurNce offers several services including vehicle insurance (car, motorcycle for example), life, health, home but also travel.

*You will find in this project*

1. A back end in C# (WebAssurNce folder)
2. A .Net Entity Framework which has generated a SQL Database 
3. A front end using React.js (assurance-app folder)

## Contributors

- Fa�dine ABDEREMANE <faidine.abderemane@gmail.com>

## License & copyright

� Fa�dine ABDEREMANE
[Linkedin](https://www.linkedin.com/in/faidineabderemane/)